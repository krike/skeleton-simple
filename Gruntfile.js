module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        folder: {
            components: 'bower_components/'
        },
        concat:
        {
            site: {
                src: [
                    '<%=folder.components%>/jquery/jquery.js',

                    // Twitter bootstrap
                    '<%=folder.components%>/bootstrap/js/alert.js',
                    '<%=folder.components%>/bootstrap/js/collapse.js',
                    '<%=folder.components%>/bootstrap/js/button.js',
                    '<%=folder.components%>/bootstrap/js/dropdown.js',
                    '<%=folder.components%>/bootstrap/js/modal.js',
                    '<%=folder.components%>/bootstrap/js/tooltip.js',
                    '<%=folder.components%>/bootstrap/js/popover.js',
                    '<%=folder.components%>/bootstrap/js/tab.js',
                    '<%=folder.components%>/bootstrap/js/transition.js',

                    // Others
                    'resources/js/*.js'

                ],
                dest: 'public/js/site.js'
            },
            modernizr: {
                src: [
                    '<%=folder.components%>/modernizr/modernizr.js'
                ],
                dest: 'public/js/modernizr.js'
            }
        },

        uglify: {
            site: {
                src: ['public/js/site.js'],
                dest: 'public/js/site.js'
            },
            modernizr: {
                src: ['public/js/modernizr.js'],
                dest: 'public/js/modernizr.js'
            }
        },
        less: {
            application: {
                options: {
                    paths: [
                        "<%=folder.components%>",
                        "resources/less"
                    ],
                    yuicompress: false
                },
                files: {
                    "public/css/screen.css": "resources/less/css-screen.less"
                }
            }
        }
    });

    // Load tasks from "grunt-sample" grunt plugin installed via Npm.
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');

    grunt.registerTask('js', ['concat']);
    grunt.registerTask('css', 'less');

    // Default task.
    grunt.registerTask('default', ['css', 'js']);
    grunt.registerTask('deploy', ['default', 'uglify']);

};